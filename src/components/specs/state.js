import home from "./home";
import about from "./about";
import howitworks from "./howitworks";
import blog from "./blog";
import faqs from "./faqs";
import termsOfServices from "./termsOfServices";
import privacyPolicy from "./privacyPolicy";

export default (function () {
    return {
        home,
        about,
        howitworks,
        blog,
        faqs,
        termsOfServices,
        privacyPolicy

    }
})();