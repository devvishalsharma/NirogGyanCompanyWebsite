
export default (
    function () {
        return {
            Main: {
                header: {
                    heading: [{ text: "Terms of Services" }],
                    description: [
                        { text: "A SmartReport is prepared solely on the basis of your blood sample and the information which the diagnostics provider shares, without access to your full medical records. Hence, you understand the Report is generic in nature." },
                        { text: "This means that (i) you must not rely on the SmartReport to diagnose or treat suspected or actual medical conditions; and (ii) you are solely responsible for any actions you do (or do not) take before and after receiving the SmartReport, and when you take (or do not take) such actions." },
                        { text: "We recommend that you always consult an appropriate medical professional for advice on your specific circumstances and situation, in particular before adopting any of the general healthcare or lifestyle advice offered in the SmartReport." }
                    ]
                }
            }
        }
    }
)();